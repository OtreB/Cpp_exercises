#include <iostream>
using namespace std;

void scambia(int* a,int* b);

int main()
{
	int x,y;
	cout<<"X=";
	cin>>x;
	cout <<"Y=";
	cin>>y;
	cout<<"l'indirizzo di X è "<<&x<<endl;
	cout<<"l'indirizzo di Y è "<<&y<<endl;
	cout<<"ORA X="<<x<<" E Y="<<y<<endl;
	scambia (&x,&y);
	cout<<"ORA X="<<x<<" E Y="<<y<<endl;
	return 0;
}



void scambia(int* a, int* b)
{
	cout<<"a="<<a<<" b="<<b<<endl;
	cout<<"*a="<<*a<<" *b="<<*b<<endl;
	
	int ausiliare=*a;
	cout<<"ausiliare="<<ausiliare<<endl;
	*a=*b;
	*b=ausiliare;
		
	}
