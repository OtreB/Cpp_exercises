#include <iostream>
#include <fstream>
#define max 21
using namespace std;

fstream schedina;

struct elem {
	
	char squadra1 [max];
	char squadra2 [max];
	int risultato; 
	elem *succ;		
	};
	
typedef elem* lista;


void inserimento (lista& inizio, elem a){
	lista p,q,r;
	for (q=inizio; q!=0 && q->risultato>a.risultato;q=q->succ) p=q;
	r= new elem;
	*r=a;
	r->succ=q;
	if (q==inizio) inizio=r;
	else p->succ=r;
	
	
	}
	
void carica_file(lista& inizio){
	schedina.open ("schedina.dat",ios::in);
	elem nuovo;
	while (schedina>>nuovo.squadra1){
		schedina>>nuovo.squadra2;
		schedina>>nuovo.risultato;
		inserimento (inizio, nuovo);
		
		}
	schedina.close();
	}


void stampa (lista inizio){
	lista q;
	for (q=inizio;q!=0;q=q->succ) cout<<q->squadra1<<" "<<q->squadra2<<" "<<q->risultato<<endl;
	
	}

void fuori_casa (lista inizio){
	schedina.open("fuori_casa.dat",ios::app);
	lista q;
	for (q=inizio;q!=0 && q->risultato==2;q=q->succ) schedina<<q->squadra2<<endl;	

			
	}

int main(int argc, char **argv)
{
	lista nuova=0;
	carica_file (nuova);
	stampa (nuova);
	fuori_casa(nuova);
	return 0;
}

